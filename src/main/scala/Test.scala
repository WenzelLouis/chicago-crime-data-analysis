import org.apache.spark.{SparkConf, SparkContext}

object Test {


  def main(args: Array[String]): Unit = {
    /**
     * SparkContext 的初始化需要一个SparkConf对象
     * SparkConf包含了Spark集群的配置的各种参数
     */
    val conf = new SparkConf().setMaster("local[*]").setAppName("test")
    //Spark程序的编写都是从SparkContext开始的
    val context = new SparkContext(conf)
    /**
     * 弹性分布式数据集（resilient distributed dataset）简称RDD
     * 他是一个元素集合,被分区地分布到集群的不同节点上,可以被并行操作
     * RDD可以从hdfs(或者任意其他的支持Hadoop的文件系统)上的一个文件开始创建,或者通过转换驱动程序中已经存在的集合得到。
     * */
    val rdd = context.makeRDD(List(12, 2, 2, 2))
    //test
    rdd.collect().foreach(println)
  }

}
