import com.csvreader.CsvReader;
import org.apache.xerces.dom.PSVIAttrNSImpl;

import java.io.FileNotFoundException;
import java.nio.charset.Charset;

public class ReadCsv {

    public static void main(String[] args) {

        long num = 0;

        try {
            CsvReader csvReader = new CsvReader("D:\\Dataset\\Crimes_-_2001_to_20211126.csv", ',', Charset.forName("utf-8"));
            while (csvReader.readRecord()){
                num++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("csv文件行数...."+num);
    }
}
